if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

alias ga='git add'
alias gd='git diff'
alias gc='git commit -m'
alias gca='git commit --amend'
alias gst='git status' alias gp='git push' alias gfp='git push --force-with-lease'
alias gri='git rebase -i origin/master'
alias grc='git rebase --continue'
alias code='cd ~/Desktop/code'
alias cdui='cd ~/Desktop/code/gitlab-ui'
alias party='cd ~/Desktop/gitlab-development-kit/gitlab'
alias tns='tmux new-session'
alias tnw='tmux new-window'
alias localip="ipconfig getifaddr en0"
alias tas="tmux attach-session"
alias trw="tmux rename-window"
alias be="bundle exec"
alias spec="bin/rspec"
alias gf="git fetch -p"
alias gb="git branch"
alias gbd="git branch -D"
alias gco="git checkout --no-guess"
alias tatt="tmux attach-session -t"
alias logz="tail -f log/development.log"

ZSH_THEME="powerlevel10k/powerlevel10k"

plugins=(
 git
 emoji
)

export SSH_KEY_PATH="~/.ssh/rsa_id"
export ZSH="/Users/laura/.oh-my-zsh"
export PATH="/usr/local/bin:$PATH"
export EDITOR="/usr/bin/vim"
export VISUAL="/usr/bin/vim"
export NVM_DIR=~/.nvm
source $(brew --prefix nvm)/nvm.sh

export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin

autoload -U compinit
compinit

source $ZSH/oh-my-zsh.sh
source /Users/laura/.zshenv
unsetopt nomatch

# The next line updates PATH for the Google Cloud SDK.
# if [ -f '/Users/laura/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/laura/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
# if [ -f '/Users/laura/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/laura/google-cloud-sdk/completion.zsh.inc'; fi
export PATH="/usr/local/sbin:$PATH"
export PATH="/usr/local/opt/icu4c/bin:$PATH"
export PATH="/usr/local/opt/icu4c/sbin:$PATH"
export PATH="/usr/local/opt/openssl@1.1/bin:$PATH"
export PATH="/usr/local/opt/node@12/bin:$PATH"
export PKG_CONFIG_PATH="/usr/local/opt/icu4c/lib/pkgconfig:$PKG_CONFIG_PATH"
export PATH="/usr/local/opt/python@3.8/bin:$PATH"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

. $HOME/.asdf/asdf.sh

source /opt/homebrew/opt/powerlevel10k/powerlevel10k.zsh-theme
