set nocompatible

if filereadable(expand("~/.vimrc.bundles"))
  source ~/.vimrc.bundles
endif

if (&t_Co > 2 || has("gui_running")) && !exists("syntax_on")
    syntax on
endif

filetype plugin indent on

let g:dracula_italic = 0
let mapleader = ","
set autoindent
set autoread
set autowrite
set backspace=2
set clipboard=unnamed
set expandtab
set history=50
set hlsearch
set ignorecase
set incsearch
set laststatus=2
set listchars=tab:»·,trail:·
set mouse=a
set nobackup
set noswapfile
set nowritebackup
set number
set numberwidth=5
set ruler
set scrolloff=5
set shiftround
set shiftwidth=2
set showcmd
set showmatch
set splitright
set splitbelow
set smartindent
set softtabstop=2
set tabstop=2
set textwidth=120
set t_Co=256
set wildmenu
set wildmode=list:longest,list:full
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,node_modules

syntax enable
colorscheme dracula

nnoremap <leader>s :%s/

inoremap jj <Esc>
noremap <Leader><Leader> <c-^>
noremap ,a :Ag<CR>

nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>""
map <leader>copy :%w !pbcopy
map <leader>F :Ag<Space>

let g:ctrlp_map = '<leader>f'

nmap <leader>= gg=G``


let g:jsx_ext_required = 0

noremap Q q
noremap q <Nop>

function! NumberToggle()
  if(&relativenumber == 1)
    set norelativenumber
  else
    set relativenumber
  endif
endfunc

au BufRead,BufNewFile *.rb setlocal textwidth=120

nnoremap <leader>nt :call NumberToggle()<cr>

"" For ale
set completeopt=menu,menuone,preview,noselect,noinsert

let g:closetag_filenames = '*.html,*.xhtml,*.phtml, *.vue'

let g:ale_completion_enabled = 1
let g:ale_lint_on_save = 0
let g:ale_completion_delay = 100
let g:ale_set_highlights = 0

let g:ale_fix_on_save = 1
nnoremap <leader>d :ALEGoToDefinition<cr>

if executable('ag')
  set grepprg=ag\ --nogroup\ --nocolor
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
  let g:ctrlp_use_caching = 0
endif
